#!/bin/bash
#
# 2018-12-21 D.S.H.
#

SNMP_COMMUNITIES="public,private"
SNMP_SYSCONTACT="admin@example.com"
SNMP_LOCATION="IT France"

ESXCLI_SNMP_SET="esxcli system snmp set"

ESXI_HOST="esxi4 esxi3 esxi2 esxi1"

for ESXI_H in ${ESXI_HOST}
do

echo "ESXi host: ${ESXI_H}"
ssh root@${ESXI_H} 'sh -s' << EOF
${ESXCLI_SNMP_SET} --enable false
${ESXCLI_SNMP_SET} --communities ${SNMP_COMMUNITIES}
${ESXCLI_SNMP_SET} --syscontact="${SNMP_SYSCONTACT}"
${ESXCLI_SNMP_SET} --syslocation="${SNMP_LOCATION}"
${ESXCLI_SNMP_SET} --hwsrc indications
${ESXCLI_SNMP_SET} --enable true
EOF

done
